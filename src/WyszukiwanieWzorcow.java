import java.io.*;


public class WyszukiwanieWzorcow {

	 
	
	private static String czytaj(String path) throws IOException
	{
        FileReader inputStream = null;
        StringBuilder sb = new StringBuilder();
        try 
        {
	     	        
	          inputStream = new FileReader(path);
	          
            int c;
            while ((c = inputStream.read()) != -1) {
            	sb.append((char)c);
         
            
            }
           int l =  sb.length();
        
            System.out.print(" dlugosc tekstu: " + l);
            return sb.toString();
    	}
	        
	        
             
        
        finally {
            if (inputStream != null) {
                inputStream.close();
            }
           
         
        }
		
	}
	
	private static long WyszukiwanieNaiwne(String tekst,String wzorzec)
	{
		
		int count=0;
		
	
		
		int DlugoscTekstu =  tekst.length() ;
		int DlugoscWzorca =  wzorzec.length() ;
	
		System.out.print("zaczynam mierzyc \n");
		long time1 = System.nanoTime();
		for(int i = 0; i < DlugoscTekstu - DlugoscWzorca; i++)
		{
		
			
			if( wzorzec.equals(tekst.substring(i,i+DlugoscWzorca))  )     //p�tla o dlugosci wzorca * petla por�wnania o dlugosci wzorca?
			{
				count++;
				System.out.print("znalaz�em" + count+ " ");
				
			}
				
			
		}
		
		long time2 = System.nanoTime();
		long elapsed = time2-time1;
		long dzielnik = 1000000;
		return elapsed/dzielnik;
	}
	
	private static long WyszukiwanieNaiwne2(String tekst,String wzorzec)
	{
		
		int count=0;
		
	
		
		int DlugoscTekstu =  tekst.length() ;
		int DlugoscWzorca =  wzorzec.length() ;
		char[] txt=tekst.toCharArray();
		char[] wzr=wzorzec.toCharArray();
		
		
		System.out.print("zaczynam mierzyc \n");
		long time1 = System.nanoTime();
		for(int i = 0; i < DlugoscTekstu - DlugoscWzorca; i++)
		{
				
			int j=0;
	
			while(j<DlugoscWzorca && txt[i+j]==wzr[j])
			{
				j++;
				
				if(j==DlugoscWzorca)
				{
					count++;
					System.out.print("znalaz�em" + count+ " ");
				}
			}
	
			
		}
		
		long time2 = System.nanoTime();
		long elapsed = time2-time1;
		
		long dzielnik = 1000000;
		return elapsed/dzielnik;
	}

	private static long RabinKarp(String tekst,String wzorzec)
	{
		
		int DlugoscTekstu =  tekst.length() ;
		int DlugoscWzorca =  wzorzec.length() ;
		
		int d = 128;  //rozmiar alfabetu
		int q = 27077; //liczba pierwsza 
		
		//StringBuilder sb = new StringBuilder();
		
		//tekst+=" ";
		
		char[] txt= tekst.toCharArray();		
		char[] wzr=wzorzec.toCharArray();
		
		int h=1;
		
	
		
			for(int i=1;i<DlugoscWzorca-1 ;i++)
			h = (h*d) % q; // wyliczy h = (d do pot�gi m-1) modulo q 
							//mno�nik 
			
			int p=0;
			int t=0;
			
			for (int i=1;i<DlugoscWzorca;i++)
					{
						p = (d*p+wzr[i])%q;
						t = (d*t+txt[i])%q;
					}
					// wyliczone: warto�� p koduj�ca P[1..m]
					// oraz warto�� t koduj�ca T[s+1..s+m] dla s==0
			
			
					// kodowanie niejednoznaczne! (haszowanie) 
			
		
			
			int t1;
			System.out.print("zaczynam mierzyc \n");
			long time1 = System.nanoTime();
			
			for (int s=0;s<DlugoscTekstu-DlugoscWzorca;s++)
			{
				if(p==t) 	//porownoje hasze
				{	
					if( wzorzec.equals(tekst.substring(s,s+DlugoscWzorca)) ) // tu por�wnujemy m znak�w (w p�tli)
					System.out.print("Znalaz�em!! ");
				}
				//czas sta�y ale dlugo zajmuje
				if (s <DlugoscTekstu-DlugoscWzorca)		// czy tekst si� nie sko�czy�?
				{
					// zmieniam hasz do uzycia w nastepnym obrocie
					
					t1=(txt[s+1]*h)%q;  //obliczam pierwszy element
					
					if (t<t1) t=t+q;
					
					t = (d*(t-t1)+txt[s+DlugoscWzorca])%q; //najpierw odejmuje element z haszu potem mno�e razy dlugos alfabetu i dodaje pierwszy element za haszem
				}
				
				
				// czyli t = (d*(t-T[s+1]*h)+T[s+1+m])%q, gdzie arytmetyka jest
				// modulo q (mno�enie i odejmowanie)
				// to wylicza warto�� t koduj�c� T[s+2, ... , s+m,s+1+m]
				// na podstawie warto�ci t koduj�cej T[s+1,s+2, ... , s+m]
			
			}
			
			long time2 = System.nanoTime();
			long elapsed = time2-time1;
			
			long dzielnik = 1000000;
			return elapsed/dzielnik;
		}
	
	private static long MorrisKnutt(String tekst,String wzorzec)
	{
		
		int DlugoscTekstu =  tekst.length() ;
		int DlugoscWzorca =  wzorzec.length() ;
			
		char[] txt= tekst.toCharArray();		
		char[] wzr=wzorzec.toCharArray();
		
		//generuje tablice
	
		int PI[] =new int[DlugoscWzorca];
		int b;
		PI[0]= b = -1; //dla zerowej dlugosci nie ma
		  
		//wypelniam tablice
		for(int i = 1; i <= DlugoscWzorca; i++)
		  {
		    while((b > -1) && (wzr[b] != wzr[i - 1])) b = PI[b];
		    
		    PI[i] = ++b; 
		  }
		
		
		//for(int i = n - 1; i > 0; i--) if(s.substr(0,i) == s.substr(n - i,i)) break;
		long sa =12;
		return sa;
	}
	

	

	
	public static void main(String[] args) throws IOException {
		
		String Tekst = czytaj("tekst.txt");
		String Wzorzec = czytaj("wzorzec.txt");

		System.out.println();
		
		long czas = WyszukiwanieNaiwne(Tekst,Wzorzec);
		System.out.print(czas);
		System.out.println();
		long czas1 = WyszukiwanieNaiwne2(Tekst,Wzorzec);
		System.out.print(czas1);
		System.out.println();
		long czas2 = RabinKarp(Tekst,Wzorzec);
		System.out.print(czas2);
		System.out.println();
		
		
	}
}
